x=5
y=5
z=60
set terminal png
set isosamples 100, 100
set samples 30, 30
set hidden3d offset 1 trianglepattern 3 undefined 1 altdiagonal bentover

set object circle at x+0.5,y+0.5,z size scr 0.005 fc rgb "navy" fillstyle solid front

set pm3d at s
set output 'output0.png'
splot'terrain.txt' matrix with lines notitle
pause -1