#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>
#include <ctime>
#include "botProject.h"
using namespace std;

void botProject::runSimulation(int rows, int cols)
{
	botProject botSim;

	//Generate terrain and resources using genTerrain function
	//Add a bot to the terrain to explore and gather resources
	botSim.genTerrain(rows, cols);
	botSim.deployBot(rows,cols);
}

