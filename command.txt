set terminal png
set isosamples 100, 100
set samples 30, 30
set hidden3d offset 1 trianglepattern 3 undefined 1 altdiagonal bentover

set pm3d at s
set output 'output0.png'
set object circle at 5+0.5,5+0.5,60 size scr 0.005 fc rgb "navy" fillstyle solid front
splot'terrain.txt' matrix with lines notitle
pause -1