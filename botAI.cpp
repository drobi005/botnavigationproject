#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <random>
#include <ctime>
#include <cmath>
#include "botAI.h"
#include "botProject.h"
using namespace std;
int botAI::pickPath(int rows, int cols, int cRow, int cCol, double steepness)
{
    //Bot will determine elevation levels in four directions
    //Function will return a number representing the chosen direction
    //0=North,1=South,2=East,3=West,4=Unable_to_move
    botAI botPathC; ///bot path choice
    int botDirection=4;
    int possibilityVal; ///used for true/false combination of directions
    double elevation;
    bool northCon, southCon, eastCon, westCon; //Conditions of all directions
    ifstream inFile;
    uniform_int_distribution<> dist(0,4);
    inFile.open("terrain.txt");
    double A[rows][cols];

    //Read in elevation levels from terrain.txt
    for(int r=0; r<rows; r++)
    {
        for(int c=0; c<cols; c++)
        {
            inFile>>elevation;
            A[r][c]=elevation;
        }
    }
    inFile.close();

    //Take the difference of elevation levels in all four directions
    //Difference result moving North
    //Direction values of -1 mean that path way cannot be traversed.
    //Out of bounds.
        ///Check if elevation difference between north and current position is less than steepnessLimit
        if (abs(A[cRow+1][cCol]-A[cRow][cCol])<steepness){
            northCon=true;
        }
        else {northCon=false;}

        ///Check if elevation difference between south and current position is less than steepnessLimit
        if (abs(A[cRow-1][cCol]-A[cRow][cCol])<steepness){
            southCon=true;
        }
        else {southCon=false;}

        ///Check if elevation difference between east and current position is less than steepnessLimit
        if (abs(A[cRow][cCol+1]-A[cRow][cCol])<steepness){
            eastCon=true;
        }
        else {eastCon=false;}

        ///Check if elevation difference between west and current position is less than steepnessLimit
        if (abs(A[cRow][cCol-1]-A[cRow][cCol])<steepness){
            westCon=true;
        }
        else {westCon=false;}



        ///There are 16 possibilities for the true/false condition combinations
        ///of the four directions.
        ///Call the conditionChecker to output a value of 0-15 to determine what
        ///paths are available.
        possibilityVal=botPathC.conditionChecker(northCon,southCon,eastCon,westCon);
        botDirection=botPathC.genDirectionVal(possibilityVal);
    return botDirection;
}
void botAI::botMove(int row, int col)
{

}
void botAI::updateBotLogInfo(int row, int col)
{

}
void botAI::updateBotCellLog(int cRow, int cCol, int botID)
{
    //stream rows and cols the bot has visited to bot###CellLog.txt
    botProject cvtToString;
    string botLogName;
    botLogName= "Bot"+cvtToString.to_string(botID)+"CellLog.txt";
    fstream botCellLog(botLogName,ios::in|ios::out|ios::app);
    if(!botCellLog.is_open())
    {
        cout << "Error while opening "+botLogName<<endl;
    }
    else
    {
        //stream out row col botID to file
        botCellLog<<cRow<<" "<<cCol<<endl;
    }

}
void botAI::checkForResources(int maxRows, int maxCols, int cRow, int cCol, int botID)
{
    //Bot will check if any resources are available in its current cell.
    //Resource information is read from terrainData.txt file
    int resRow=1000, resCol=1000, resType=5;
    char resSize;
    botProject cvtToString;
    string botResLog;
    botResLog= "Bot"+cvtToString.to_string(botID)+"ResourceLog.txt";
    bool condition=true;
    fstream inFile;
    inFile.open("terrainData.txt");
    while(condition)
    {
        inFile>>resRow>>resCol>>resType>>resSize;
        if ((cRow==resRow)&&(cCol==resCol))
        {
            condition=false;
        }
    }

    //Create a resource log listing bots findings
    if (resType==0)
    {
        fstream botRLog(botResLog,ios::in|ios::out|ios::app);
        if(!botRLog.is_open())
        {
            cout << "Error while opening "+botResLog<<endl;
        }
        else
        {
            //Stream out row col botID to file
            botRLog<<"Bot "<<botID<<" did not find resources at "<<cRow <<" "<<cCol<<endl;
        }
    }
    else
    {
        fstream botRLog(botResLog,ios::in|ios::out|ios::app);
        if(!botRLog.is_open())
        {
            cout << "Error while opening "+botResLog<<endl;
        }
        else
        {
            //Stream out row col botID to file
            botRLog<<"Bot "<<botID<<" found resource type "<<resType<<", size "<< resSize<< ", at " <<cRow <<" "<<cCol<<endl;
        }
    }



}
void botAI::checkForBots()
{

}
void botAI::exchangeInfo()
{

}

int botAI::conditionChecker(bool northCon,bool southCon,bool eastCon,bool westCon)
{
    ///Truth table of all the possible true/false combinations done on paper
    ///a return value of 0 represent condition TTTT, 1 represents condition TTTF...
    ///15 represents condition FFFF
    int conditionValue;
    if ((northCon==true)&&(southCon==true)&&(eastCon==true)&&(westCon==true))
    {
        conditionValue=0;
    }
    else if ((northCon==true)&&(southCon==true)&&(eastCon==true)&&(westCon==false))
    {
        conditionValue=1;
    }
    else if ((northCon==true)&&(southCon==true)&&(eastCon==false)&&(westCon==true))
    {
        conditionValue=2;
    }
    else if ((northCon==true)&&(southCon==true)&&(eastCon==false)&&(westCon==false))
    {
        conditionValue=3;
    }
    else if ((northCon==true)&&(southCon==false)&&(eastCon==true)&&(westCon==true))
    {
        conditionValue=4;
    }
    else if ((northCon==true)&&(southCon==false)&&(eastCon==true)&&(westCon==false))
    {
        conditionValue=5;
    }
    else if ((northCon==true)&&(southCon==false)&&(eastCon==false)&&(westCon==true))
    {
        conditionValue=6;
    }
    else if ((northCon==true)&&(southCon==false)&&(eastCon==false)&&(westCon==false))
    {
        conditionValue=7;
    }
    else if ((northCon==false)&&(southCon==true)&&(eastCon==true)&&(westCon==true))
    {
        conditionValue=8;
    }
    else if ((northCon==false)&&(southCon==true)&&(eastCon==true)&&(westCon==false))
    {
        conditionValue=9;
    }
    else if ((northCon==false)&&(southCon==true)&&(eastCon==false)&&(westCon==true))
    {
        conditionValue=10;
    }
    else if ((northCon==true)&&(southCon==true)&&(eastCon==true)&&(westCon==false))
    {
        conditionValue=11;
    }
    else if ((northCon==false)&&(southCon==false)&&(eastCon==true)&&(westCon==true))
    {
        conditionValue=12;
    }
    else if ((northCon==false)&&(southCon==false)&&(eastCon==true)&&(westCon==false))
    {
        conditionValue=13;
    }
    else if ((northCon==false)&&(southCon==false)&&(eastCon==false)&&(westCon==true))
    {
        conditionValue=14;
    }
    else if ((northCon==false)&&(southCon==false)&&(eastCon==false)&&(westCon==false))
    {
        conditionValue=15;
    }
    return conditionValue;
}
int botAI::genDirectionVal(int possibilityVal)
{
    int directionVal;
    srand(time(0));
    ///generate a value indicating the path the bot will pick based on the the 0-15
    ///possibilityVal. Random number generator used to pick one of four true paths.
    ///0=North,1=South,2=East,3=West,4=Unable_to_move
    switch(possibilityVal)
    {
    case 0:
        directionVal = rand()%4+0;
        break;
    case 1:
        directionVal = rand()%4+0;
        while(directionVal!=3){
            directionVal = rand()%4+0;
        }
        break;
    case 2:
        directionVal = rand()%4+0;
        while(directionVal!=2){
            directionVal = rand()%4+0;
        }
        break;
    case 3:
        directionVal = rand()%4+0;
        while((directionVal!=2)&&(directionVal!=3)){
            directionVal = rand()%4+0;
        }
        break;
    case 4:
        directionVal = rand()%4+0;
        while(directionVal!=1){
            directionVal = rand()%4+0;
        }
        break;
    case 5:
        directionVal = rand()%4+0;
        while((directionVal!=1)&&(directionVal!=3)){
            directionVal = rand()%4+0;
        }
        break;
    case 6:
        directionVal = rand()%4+0;
        while((directionVal!=1)&&(directionVal!=2)){
            directionVal = rand()%4+0;
        }
        break;
    case 7:
        directionVal = 0;
        break;
    case 8:
        directionVal = rand()%4+0;
        while(directionVal!=0){
            directionVal = rand()%4+0;
        }
        break;
    case 9:
        directionVal = rand()%4+0;
        while((directionVal!=0)&&(directionVal!=3)){
            directionVal = rand()%4+0;
        }
        break;
    case 10:
        directionVal = rand()%4+0;
        while((directionVal!=0)&&(directionVal!=2)){
            directionVal = rand()%4+0;
        }
        break;
    case 11:
        directionVal = 1;
        break;
    case 12:
        directionVal = rand()%4+0;
        while((directionVal!=0)&&(directionVal!=1)){
            directionVal = rand()%4+0;
        }
        break;
    case 13:
        directionVal = 2;
        break;
    case 14:
        directionVal = 3;
        break;
    case 15:
        directionVal = 4; ///bot is stuck
        break;
    }
    return directionVal;
}
