#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <sstream>
#include "botProject.h"
#include "botAI.h"
using namespace std;

void botProject::deployBot(int rows, int cols)
{
    botAI bot1;
    botProject bot1CMDFile;
    srand(time(0));
    //Store elevation data from terrain.txt into the A array
    int numOfBots;
    double elevation;
    ifstream inFile;
    inFile.open("terrain.txt");
    double A[rows][cols];

    for(int r=0; r<rows; r++)
    {
        for(int c=0; c<cols; c++)
        {
            inFile>>elevation;
            A[r][c]=elevation;
        }
    }
    inFile.close();


    //prompt to enter how many bots to deploy on terrain
    numOfBots=numberOfBots();


    switch(numOfBots)
    {
        case 1:
            //place 1 bot at random point on terrain
            int localB1Row, localB1Col, b1ID, b1Rank, botMoves, botDirection;
            double steepnessLimit;
            localB1Row=rand()%(rows+1)+0;
            localB1Col=rand()%(cols+1)+0;
            b1ID=rand()%1000+0;
            b1Rank=rand()%100+0;
            bot1.setBotInfo(localB1Row, localB1Col, b1ID, b1Rank);
            cout<<"How many times do you want the bots to move? ";
            cin>>botMoves;
            cout<<"What is the max level of steepness a bot can climb(50.5 recommended)? ";
            cin>>steepnessLimit; //50.5 default value

            for(int i = 0; i<botMoves; i++)
            {
                bot1.updateBotCellLog(bot1.getCurrentRow(), bot1.getCurrentCol(), bot1.getBotID());
                bot1.checkForResources(rows, cols, bot1.getCurrentRow(), bot1.getCurrentCol(), bot1.getBotID());
                //0=North,1=South,2=East,3=West,4=Unable_to_move
                botDirection= bot1.pickPath(rows, cols, bot1.getCurrentRow(), bot1.getCurrentCol(), steepnessLimit);
                if(botDirection==0)
                {
                    localB1Row+=1;
                    bot1.setBotInfo(localB1Row, localB1Col, b1ID, b1Rank);
                }
                else if(botDirection==1)
                {
                    localB1Row-=1;
                    bot1.setBotInfo(localB1Row, localB1Col, b1ID, b1Rank);
                }
                else if(botDirection==2)
                {
                    localB1Col+=1;
                    bot1.setBotInfo(localB1Row, localB1Col, b1ID, b1Rank);
                }
                else if(botDirection==3)
                {
                    localB1Col-=1;
                    bot1.setBotInfo(localB1Row, localB1Col, b1ID, b1Rank);
                }
                else if(botDirection==4)
                {
                    bot1.setBotInfo(localB1Row, localB1Col, b1ID, b1Rank);
                    cout<<"A bot is stuck and cannot move"<<endl;
                }
                //Clear contents and/or create botSimulation.txt
                bot1CMDFile.updateCMDFile(i,localB1Row,localB1Col,A[localB1Row][localB1Col]);
                system("gnuplot botSimulation.txt"); ///called here to generate proper png images of bot's movements across terrain
            }


            break;
        case 2:
            //place 1 bot at random point on terrain
            break;
        case 3:
            //place 1 bot at random point on terrain
            break;
        case 4:
            //place 1 bot at random point on terrain
            break;
        case 5:
            //place 1 bot at random point on terrain
            break;
    }


}
string botProject::to_string(int i)
{
    stringstream ss;
    ss<<i;
    return ss.str();
}

int botProject::numberOfBots()
{
    bool inputCheck;
    int numOfBots;
    cout<<"\n\n\n\nHow many bots do you need to deploy(Enter 1-5)? [Only 1 bot works] ";
    inputCheck=true;
    cin>>numOfBots;
    while(inputCheck)
    {
        if ((numOfBots >=1) && (numOfBots <=5))
        {
            inputCheck=false;
        }
        else
        {
            cout<<"Invalid input! How many bots do you need to deploy(Enter 1-5)[Only 1 works]? ";
            cin>>numOfBots;
        }
    }
    cout<<numOfBots<<" bot(s) will be deployed"<<endl;
    return numOfBots;
}
void botProject::updateCMDFile(int i, int lRow, int lCol, double height)
{
    ///This function is used to clear contents of the command.txt file to update the bots
    ///xyz position and png image file number through each i iteration. That way the plotted
    ///bot points appear separately with each png image.
    ofstream fout;
    fout.open("botSimulation.txt");
    fout<<"x="<<lRow<<endl;
    fout<<"y="<<lCol<<endl;
    fout<<"z="<<height<<endl;
    fout<<"set terminal png"<<endl;
    fout<<"set isosamples 100, 100"<<endl;
    fout<<"set samples 30, 30"<<endl;
    fout<<"set hidden3d offset 1 trianglepattern 3 undefined 1 altdiagonal bentover"<<endl;

    fout<<"set pm3d at s"<<endl;
    fout<<"set output \'output"<<i<<".png\'"<<endl;
    fout<<"set object circle at x+0.5,y+0.5,z size scr 0.005 fc rgb \"navy\" fillstyle solid front"<<endl;
    fout<<"splot'terrain.txt' matrix with lines notitle"<<endl;
}
