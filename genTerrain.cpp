#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>
#include <ctime>
#include "botProject.h"
///Description: Random number generator is used to generate random elevation value across an nx*ny terrain.
///             The elevations are streamed into terrain.txt. Random resources are generated at random
///             locations on the map. The resource coordinates, type and size are streamed to
///             terrainData.txt.
///             Resources identified as numbers from 1-5 and sizes from S,M, or L. Type 1 means cell does not
///             have any resources.
using namespace std;

void botProject::genTerrain(int rows, int cols)
{
    ///generate plane with row by col terrain with various levels of elevation
    default_random_engine gen(time(NULL));
    normal_distribution<double> ndist(0,3);
    normal_distribution<double> n2dist(10,3);
    uniform_int_distribution<int> hnumdist(5,10);

    int hills;
    int hx,hy;

    cout<<"Rows = "<<rows<<" Columns = "<<cols<< endl;
    double ** A;
    A =new double *[rows];

    for(int i=0; i<rows; i++)
    {
        A[i]=new double[cols];
    }

    ///initial plane
    for(int r=0; r<rows; r++)
    {
        for(int c=0; c<cols; c++)
        {
           A[r][c]=r+(cols-c)+ndist(gen);
        }

    }

    ///generate hills or plateaus on plane using rng engine
    uniform_int_distribution<int>hxdist(0,cols-1);
    uniform_int_distribution<int> hydist(0,rows-1);
    uniform_int_distribution<int> dice(1,6);
    hills=hnumdist(gen);
    cout<<"Number of hills: "<<hills<<endl;
    int d;
    double fac;
    for(int h=0; h<hills; h++)
    {           d=dice(gen);
                if(d==1){fac=-10;}
                else if(d>4){fac=3;}
                else {fac=1;}
                cout<<"Hill factor: "<<fac<<endl;
        hx=hxdist(gen);
        hy=hydist(gen);
        for(int i=hx; i<hx+(cols/5); i++ )
        {cout<<" i= "<<i<<": ";

            for(int j=hy; j<hy+(rows/5); j++)
            {
                if((j<rows)&&(i<cols))
                {cout<<j<<" ";

                A[i][j]=A[i][j]+fac*n2dist(gen);}

            }
            cout<<endl;
        }

    }

    ///store elevations levels at each row and col in terrain.txt
    ofstream fout;
    fout.open("terrain.txt");

    for(int r=0; r<rows; r++)
    {
        for(int c=0; c<cols; c++)
        {
           fout<<A[r][c]<<" ";
        }
        fout<<endl;
    }
    fout.close();

    ///store row-col-resource values in terrainData.txt
    fout.open("terrainData.txt");
    srand(time(0));
    char B[]={'S','M','L'};
    for(int r=0; r<rows; r++)
    {
        for(int c=0; c<cols; c++)
        {
           //fout<<r<<" "<<c<<" "<<A[r][c]<<" "<<rand()%5+0<< " "<< B[rand()%3+0]<<endl;
           fout<<r<<" "<<c<<" "<<rand()%5+0<< " "<< B[rand()%3+0]<<endl;
        }
        fout<<endl;
    }
    fout.close();


    //system("gnuplot command.txt");
}

