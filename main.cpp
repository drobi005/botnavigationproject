//Demetrius Robinson
//Project 2: Bot Project
//March 14, 2018
//Description: Program generates a terrain containing hills, plateaus, and elevation. Resources
//             are scattered throughout the terrain. A number of bots are then deployed at random
//             locations on the terrain to explore and gather as many resources possible.
//
//
//
//
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>
#include <ctime>
#include "botProject.h"
using namespace std;

int main()
{
    botProject botPro;

    //Run the bot simulation
    int rows, cols;
    cout<<"Enter number of Rows: ";
    cin>>rows;
    cout<<"Enter number of Columns: ";
    cin>>cols;
    botPro.setRowAndCol(rows, cols);
    botPro.runSimulation(botPro.getRow(), botPro.getCol());

    return 0;
}

