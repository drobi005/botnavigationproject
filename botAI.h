#include <iostream>
using namespace std;
class botAI
{

public:
    botAI(){}
    botAI(int cRow, int cCol, int botID, int botRank)
    {localRow=cRow; localCol=cCol; bID=botID; bRank=botRank;}
    ~botAI(){}
    void display(){}

    int getCurrentRow(){return localRow;}
    int getCurrentCol(){return localCol;}
    int getBotID(){return bID;}
    int getBotRank(){return bRank;}

    void setBotInfo(int row, int col, int botID, int botRank)
    {localRow=row; localCol=col; bID=botID; bRank=botRank;}
    int pickPath(int maxRows, int maxCols, int cRow, int cCol, double steepness);
	void botMove(int row, int col);
	void updateBotLogInfo(int row, int col);
	void updateBotCellLog(int row, int col, int botID);
    void checkForResources(int maxRow, int maxCol, int cRow, int cCol, int botID);
    void checkForBots();
    void exchangeInfo();
    int conditionChecker(bool northCon,bool southCon,bool eastCon,bool westCon);
    int genDirectionVal(int possibilityVal);
private:
    int localRow;
    int localCol;
    int bID;
    int bRank;

};
