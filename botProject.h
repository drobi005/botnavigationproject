#include <iostream>
using namespace std;
class botProject
{

public:
    botProject(){}
    botProject(int nRow, int nCol, int cellID)
    {nR=nRow; nC=nCol; cID=cellID;}
    ~botProject(){}
    void display(){}

    int getRow(){return nR;}
    int getCol(){return nC;}
    int getCellID(){return cID;}
    void setRowAndCol(int row, int col){nR=row; nC=col;}
    void genTerrain(int row, int col);      //called in genTerrain.cpp
	void runSimulation(int row, int col);   //called in botSimulation.cpp
	void deployBot(int row, int col);   //called in botProject.cpp
	int numberOfBots();
	string to_string(int i);
	void updateCMDFile(int i, int lRow, int lCol, double height);
private:
    int nR;
    int nC;
    int cID;

};
